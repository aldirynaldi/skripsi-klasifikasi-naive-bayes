<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Dataset extends Model
{
    protected $table = "datasets";
    protected $primaryKey = "id";
    public $timestamps = false;
    protected $fillable = [
        'umur', 
        'jk',
        'hsl_survei', 
        'jml_pinjaman', 
        'jaminan', 
        'durasi', 
        'modal', 
        'ekonomi',
        'status'
    ];
}
