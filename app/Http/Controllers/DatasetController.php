<?php

namespace App\Http\Controllers;

use App\Imports\DatasetImport;
use App\Dataset;
use Illuminate\Http\Request;
use Excel;

use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;

use robertogallea\LaravelPython\Services\LaravelPython;

//sweet alert
use RealRashid\SweetAlert\Facades\Alert;


class DatasetController extends Controller
{
    public function import(Request $request) {
        
        $file = $request->file('file');
        $namaFile = $file->getClientOriginalName();
        $file->move('Dataset', $namaFile);

        Excel::import(new DatasetImport, public_path('/Dataset/'.$namaFile));


        return redirect('/data')->with('success', 'berhasil di import');
    }

    public function store(Request $request){
        //dd($request->umur);
        $data = Dataset::create([
            'umur' => $request->umur, 
            'jk' => $request->jk,
            'hsl_survei' => $request->hsl_survei, 
            'jml_pinjaman' => $request->jml_pinjaman, 
            'jaminan' => $request->jaminan, 
            'durasi' => $request->durasi, 
            'modal' => $request->modal, 
            'ekonomi' => $request->ekonomi,
            'status' => $request->status
        ]);
        return redirect('/data')->with('success', 'berhasil di tambahkan');
    }

    public function index() {
        $datasets = Dataset::All();

        //dd($datasets);
        // $dataUmur = [];
        // $dataJk = [];
        // $dataSurvei =[];
        // $dataJmlPinjaman = [];
        // $datajaminan = [];
        // $dataDurasi = [];
        // $dataModal = [];
        // $dataEkonomi = [];
        // $dataStatus = [];


        foreach ($datasets as $i => $data) {
            # code...

            //umur
            if ($data->umur == 1) {
                $data->umur = 'muda';
            }
            elseif($data->umur == 2) {
                $data->umur = 'dewasa';
            }
            elseif($data->umur == 3) {
                $data->umur = 'tua';
            }

            //jenis kelamin
            if ($data->jk == 1) {
                $data->jk = 'laki-laki';
            }
            elseif($data->jk == 2) {
                $data->jk = 'perempuan';
            }

            //hasil survei
            if ($data->hsl_survei == 1) {
                $data->hsl_survei = 'Levl A';
            }elseif ($data->hsl_survei == 2) {
                $data->hsl_survei = 'Levl B';
            }elseif ($data->hsl_survei == 3) {
                $data->hsl_survei = 'Levl C';
            }elseif ($data->hsl_survei == 4) {
                $data->hsl_survei = 'Levl D';
            }

            //Jumlah Pinjaman
            if ($data->jml_pinjaman == 1) {
                $data->jml_pinjaman = 'A';
            }elseif ($data->jml_pinjaman == 2) {
                $data->jml_pinjaman = 'B';
            }elseif ($data->jml_pinjaman == 3) {
                $data->jml_pinjaman = 'C';
            }elseif ($data->jml_pinjaman == 4) {
                $data->jml_pinjaman = 'D';
            }elseif ($data->jml_pinjaman == 5) {
                $data->jml_pinjaman = 'E';
            }

            //jaminan
            if ($data->jaminan == 1) {
                $data->jaminan = 'jmn A';
            }elseif ($data->jaminan == 2) {
                $data->jaminan = 'jmn B';
            }elseif ($data->jaminan == 3) {
                $data->jaminan = 'jmn C';
            }

            //Durasi
            if ($data->durasi == 1) {
                $data->durasi = 'cepat';
            }elseif ($data->durasi == 2) {
                $data->durasi = 'normal';
            }elseif ($data->durasi == 3) {
                $data->durasi = 'lama';
            }

            //Modal
            if ($data->modal == 1) {
                $data->modal = 'ada';
            }elseif ($data->modal == 2) {
                $data->modal = 'tidak ada';
           }

           //Ekonomi
            if ($data->ekonomi == 1) {
                $data->ekonomi = 'cukup';
            }elseif ($data->ekonomi == 2) {
                $data->ekonomi = 'belum cukup';
            }

            //Keputusan Kredit
            

        }

        //dd($datasets);
        return view('contents.dataset.index',compact('datasets'));
    }

    public function edit($id) {
        $data = Dataset::find($id);
        return view('contents.dataset.edit',compact('data'));
    }

    public function update(Request $request, $id) {
        Dataset::find($id)->update([
            'umur' => $request->umur, 
            'jk' => $request->jk,
            'hsl_survei' => $request->hsl_survei, 
            'jml_pinjaman' => $request->jml_pinjaman, 
            'jaminan' => $request->jaminan, 
            'durasi' => $request->durasi, 
            'modal' => $request->modal, 
            'ekonomi' => $request->ekonomi,
            'status' => $request->status
        ]);
       
        return redirect('/data');
    }

    public function kredit() {
        $datasets = Dataset::All();
        //dd($datasets);


        return view('contents.klasifikasi.kredit',compact('datasets'));
    }

    public function single() {
        $datasets = Dataset::All();
        //dd($datasets);
        return view('contents.klasifikasi.single',compact('datasets'));
    }

    public function destroy($id){
        Dataset::find($id)->delete();
        
        Alert::success('Berhasil terhapus', 'Data berhasil terhapus');

        return redirect('/data');
    }
}
