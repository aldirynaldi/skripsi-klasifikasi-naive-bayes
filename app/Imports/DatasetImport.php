<?php

namespace App\Imports;

use App\Dataset;
use Maatwebsite\Excel\Concerns\ToModel;

class DatasetImport implements ToModel
{
    
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new Dataset([
           'umur'           => $row[0],
           'jk'             => $row[1], 
           'hsl_survei'     => $row[2], 
           'jml_pinjaman'   => $row[3], 
           'jaminan'        => $row[4], 
           'durasi'         => $row[5], 
           'modal'          => $row[6], 
           'ekonomi'        => $row[7],
           'status'         => $row[8], 
        ]);
    }
}
