@extends('layouts.master')

@section('content')

  <!-- Modal import csv -->
  <div class="modal fade" id="importCsv" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Upload Dataset CSV</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          ..
          <form action="import" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="input-group mb-3">
                <label>File upload</label>
                <input type="file" name="file" class="form-control file-upload-default">

                <div class="input-group col-xs-12">
                  <span class="input-group-append">
                    <button class="file-upload-browse btn btn-primary" type="submit">Upload</button>
                  </span>
                </div>

            </div>
          </form>
          
        </div>
      </div>
    </div>
  </div>

  <!-- Modal create data-->
  <div class="modal fade" id="createCategory" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="staticBackdropLabel">add New Data</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <!-- form start -->
        <form role="form" action="/data/store" method="Post">
          @csrf
          <div class="modal-body">
            <div class="form-group">

              <div class="form-group">
                <label for="title">Umur</label>
                <select name="umur" class="form-control form-control-lg" id="formSelectUmur">
                  <option value="1">muda</option>
                  <option value="2">dewasa</option>
                  <option value="3">tua</option>
                </select>
                {{-- <input type="text" class="form-control" id="categorie" name="categorie" placeholder="Name categorie"> --}}
              </div>
              
              <div class="form-group">
                <label for="title">Jenis Kelamin</label>
                <select name="jk" class="form-control form-control-lg" id="formSelectJK">
                  <option value="1">laki - laki</option>
                  <option value="2">perempuan</option>
                </select>
                {{-- <input type="text" class="form-control" id="categorie" name="categorie" placeholder="Name categorie"> --}}
              </div>

              <div class="form-group">
                <label for="title">Hasil Survei</label>
                <select name="hsl_survei" class="form-control form-control-lg" id="formSelectHslSurvei">
                  <option value="1">Levl A</option>
                  <option value="2">Levl B</option>
                  <option value="3">Levl C</option>
                  <option value="4">Levl D</option>
                </select>
                {{-- <input type="text" class="form-control" id="categorie" name="categorie" placeholder="Name categorie"> --}}
              </div>
              
              <div class="form-group">
                <label for="title">Jumlah Pinjaman</label>
                <select name="jml_pinjaman" class="form-control form-control-lg" id="formSelectJmlPinjaman">
                  <option value="1">A</option>
                  <option value="2">B</option>
                  <option value="3">C</option>
                  <option value="4">D</option>
                  <option value="4">E</option>
                </select>
                {{-- <input type="text" class="form-control" id="categorie" name="categorie" placeholder="Name categorie"> --}}
              </div>

              <div class="form-group">
                <label for="title">Jaminan</label>
                <select name="jaminan" class="form-control form-control-lg" id="formSelectJaminan">
                  <option value="1">jmn A</option>
                  <option value="2">jmn B</option>
                  <option value="3">jmn C</option>
                </select>
                {{-- <input type="text" class="form-control" id="categorie" name="categorie" placeholder="Name categorie"> --}}
              </div>
              
              <div class="form-group">
                <label for="title">Durasi</label>
                <select name="durasi" class="form-control form-control-lg" id="formSelectDurasi">
                  <option value="1">cepat</option>
                  <option value="2">normal</option>
                  <option value="3">lama</option>
                </select>
                {{-- <input type="text" class="form-control" id="categorie" name="categorie" placeholder="Name categorie"> --}}
              </div>
              
              <div class="form-group">
                <label for="title">Modal</label>
                <select name="modal" class="form-control form-control-lg" id="formSelectModal">
                  <option value="1">ada</option>
                  <option value="2">tidak ada</option>
                </select>
                {{-- <input type="text" class="form-control" id="categorie" name="categorie" placeholder="Name categorie"> --}}
              </div>
              
              <div class="form-group">
                <label for="title">Ekonomi</label>
                <select name="ekonomi" class="form-control form-control-lg" id="formSelectEkonomi">
                  <option value="1">cukup</option>
                  <option value="2">belum cukup</option>
                </select>
                {{-- <input type="text" class="form-control" id="categorie" name="categorie" placeholder="Name categorie"> --}}
              </div>
              
              <div class="form-group">
                <label for="title">Status</label>
                <select name="status" class="form-control form-control-lg" id="formSelectEkonomi">
                  <option value="1">Yes</option>
                  <option value="0">No</option>
                </select>
                {{-- <input type="text" class="form-control" id="categorie" name="categorie" placeholder="Name categorie"> --}}
              </div>
              
            </div>
          </div>

          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Submit</button>
          </div>

        </form>
        
        
      </div>
    </div>
  </div>

    <h1>Dataset</h1>
    <div class="card">
        <div class="card-body">
          <h4 class="card-title">Basic Table</h4>   

          <!-- Button trigger modal -->
          <button type="button" class="btn btn-lg btn-info font-weight-bold" data-toggle="modal" data-target="#importCsv">+ Upload CSV</button>

          <!-- Button trigger modal -->
          <button class="btn btn-primary btn-lg font-weight-bold" data-toggle="modal" data-target="#createCategory">Add New Categorie</button>
          
          <div class="table-responsive mt-4">
            <table class="table">
              <thead>
                <tr class="text-center">
                  <th>No</th>
                  <th>Umur</th>
                  <th>Jenis Kelamin</th>
                  <th>hasil Survei</th>
                  <th>Jumlah Pinjaman</th>
                  <th>Jaminan</th>
                  <th>Durasi</th>
                  <th>Modal</th>
                  <th>Ekonomi</th>
                  <th>Status</th>
                  <th>Aksi</th>
                </tr>
              </thead>
              <tbody>
                @foreach ($datasets as $i => $data)
                    <tr class="text-center">
                        <td>{{ $i+1 }}</td>

                        <td>{{ $data->umur }}</td>
                        <td>{{ $data->jk }}</td>
                        <td>{{ $data->hsl_survei }}</td>
                        <td>{{ $data->jml_pinjaman }}</td>
                        <td>{{ $data->jaminan }}</td>
                        <td>{{ $data->durasi }}</td>
                        <td>{{ $data->modal }}</td>
                        <td>{{ $data->ekonomi }}</td>
                        
                        @if ($data->status == 1)
                            <td><label class="badge badge-info">Yes</label></td>
                        @else
                            <td><label class="badge badge-danger">No</label></td>
                        @endif

                        <td class="d-flex">
                          <!-- Button trigger modal -->
                          <a href="/data/{{ $data->id }}/edit" class="btn btn-success mr-2">Edit</a>

                          <form action="/data/{{ $data->id }}" method="POST">
                            @csrf
                            @method('delete')
                            <input type="submit" class="btn btn-danger" value="Delete">
                          </form>

                        </td>
                    </tr>
                    
                @endforeach
                
              </tbody>
            </table>
          </div>
        </div>
    </div>
@endsection
