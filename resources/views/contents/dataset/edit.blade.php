@extends('layouts.master')

@section('content')
<div class="card">
    <div class="card-body">
      <h4 class="card-title">Default form</h4>
      <p class="card-description">
        Basic form layout
      </p>

        <!-- form start -->
        <form role="form" action="/data/{{$data->id}}" class="forms-sample" method="Post">
            @csrf
            @method('put')
            <div class="form-group">

                <div class="form-group">
                    <label for="title">Umur</label>
                    <select name="umur" class="form-control form-control-lg" id="formSelectUmur">
                        <option value="1"  @if ($data->umur == 1) selected @endif >muda</option>
                        <option value="2"  @if ($data->umur == 2) selected @endif >dewasa</option>
                        <option value="3"  @if ($data->umur == 3) selected @endif >tua</option>
                    </select>
                </div>
                
                <div class="form-group">
                    <label for="title">Jenis Kelamin</label>
                    <select name="jk" class="form-control form-control-lg" id="formSelectJK">
                        <option value="1" @if ($data->jk == 1) selected @endif >laki - laki</option>
                        <option value="2" @if ($data->jk == 2) selected @endif >perempuan</option>
                    </select>
                </div>

                <div class="form-group">
                    <label for="title">Hasil Survei</label>
                    <select name="hsl_survei" class="form-control form-control-lg" id="formSelectHslSurvei">
                        <option value="1" @if ($data->hsl_survei == 1) selected @endif >Levl A</option>
                        <option value="2" @if ($data->hsl_survei == 2) selected @endif >Levl B</option>
                        <option value="3" @if ($data->hsl_survei == 3) selected @endif >Levl C</option>
                        <option value="4" @if ($data->hsl_survei == 4) selected @endif >Levl D</option>
                    </select>
                </div>
                
                <div class="form-group">
                    <label for="title">Jumlah Pinjaman</label>
                    <select name="jml_pinjaman" class="form-control form-control-lg" id="formSelectJmlPinjaman">
                        <option value="1" @if ($data->jml_pinjaman == 1) selected @endif >A</option>
                        <option value="2" @if ($data->jml_pinjaman == 2) selected @endif >B</option>
                        <option value="3" @if ($data->jml_pinjaman == 3) selected @endif >C</option>
                        <option value="4" @if ($data->jml_pinjaman == 4) selected @endif >D</option>
                        <option value="4" @if ($data->jml_pinjaman == 5) selected @endif >E</option>
                    </select>
                </div>

                <div class="form-group">
                    <label for="title">Jaminan</label>
                    <select name="jaminan" class="form-control form-control-lg" id="formSelectJaminan">
                        <option value="1" @if ($data->jaminan == 1) selected @endif >jmn A</option>
                        <option value="2" @if ($data->jaminan == 2) selected @endif >jmn B</option>
                        <option value="3" @if ($data->jaminan == 3) selected @endif >jmn C</option>
                    </select>
                </div>
                
                <div class="form-group">
                    <label for="title">Durasi</label>
                    <select name="durasi" class="form-control form-control-lg" id="formSelectDurasi">
                        <option value="1" @if ($data->durasi == 1) selected @endif >cepat</option>
                        <option value="2" @if ($data->durasi == 2) selected @endif >normal</option>
                        <option value="3" @if ($data->durasi == 3) selected @endif >lama</option>
                    </select>
                </div>
                
                <div class="form-group">
                    <label for="title">Modal</label>
                    <select name="modal" class="form-control form-control-lg" id="formSelectModal">
                        <option value="1" @if ($data->modal == 1) selected @endif >ada</option>
                        <option value="2" @if ($data->modal == 2) selected @endif >tidak ada</option>
                    </select>
                </div>
                
                <div class="form-group">
                    <label for="title">Ekonomi</label>
                    <select name="ekonomi" class="form-control form-control-lg" id="formSelectEkonomi">
                        <option value="1" @if ($data->ekonomi == 1) selected @endif >cukup</option>
                        <option value="2" @if ($data->ekonomi == 2) selected @endif >belum cukup</option>
                    </select>
                </div>
                
                <div class="form-group">
                    <label for="title">Status</label>
                    <select name="status" class="form-control form-control-lg" id="formSelectEkonomi">
                        <option value="1" @if ($data->status == 1) selected @endif >Yes</option>
                        <option value="0" @if ($data->status == 0) selected @endif >No</option>
                    </select>
                </div>
                
            </div>

            <button type="submit" class="btn btn-primary">Submit</button>

        </form>

    </div>
  </div>
@endsection