@extends('layouts.master')

@section('content')

    <h1>klasifikasi</h1>
    <div class="row">
      <div class="col-lg-6 grid-margin stretch-card">
        <div class="card">
          <div class="card-body">
            <h4 class="card-title">Train Table</h4>
            <div class="table-responsive">
              <table class="table table-paginate">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Umur</th>
                    <th>Jenis Kelamin</th>
                    <th>hasil Survei</th>
                    <th>Jumlah Pinjaman</th>
                    <th>Jaminan</th>
                    <th>Durasi</th>
                    <th>Modal</th>
                    <th>Ekonomi</th>
                    <th>Status</th>
                  </tr>
                </thead>
                <tbody id="itemsTrain">
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>

      <div class="col-lg-6 grid-margin stretch-card">
        <div class="card">
          <div class="card-body">
            <h4 class="card-title">Testing Table</h4>
            <div class="table-responsive">
              <table class="table table-paginate">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Umur</th>
                    <th>Jenis Kelamin</th>
                    <th>hasil Survei</th>
                    <th>Jumlah Pinjaman</th>
                    <th>Jaminan</th>
                    <th>Durasi</th>
                    <th>Modal</th>
                    <th>Ekonomi</th>
                    <th>Status</th>
                  </tr>
                </thead>
                <tbody id="itemsTest">
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>

      <div class="col-lg-12 grid-margin stretch-card">
        <div class="card">
          <div class="card-body">
            <h4 class="card-title">Prediction Table</h4>
            <div class="table-responsive">
              <table class="table">
                <thead id="t_headPred">
                </thead>
                <tbody id="t_bodyPred">
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>

      <div class="col-lg-12 grid-margin stretch-card">
        <div class="card">
          <div class="card-body">
            <h4 class="card-title">Probability table</h4>
            <div class="table-responsive pt-3">
              <table class="table table-bordered">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>Yes</th>
                    <th>No</th>
                  </tr>
                </thead>
                <tbody id="prob">
                  
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>

      <div class="col-sm-12 grid-margin stretch-card">
        <div class="card">
          <div class="card-body">
              <h4 class="card-title">Akurasi</h4>
              <p>tahap akurasi dengan metode naive bayes</p>
              <h4 id="akurasi" class="text-dark font-weight-bold mb-2">0</h4>
          </div>
        </div>
      </div>


    </div>

    <script>
      const data = {!! $datasets !!};
      //console.log(data);
    </script>
    
    <script src=" {{ asset('js/klasifikasi.js') }} "></script>



@endsection