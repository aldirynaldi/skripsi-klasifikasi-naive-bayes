@extends('layouts.master')

@section('content')
    <div class="card">
        <div class="card-body">
        <h4 class="card-title">Default form</h4>
        <p class="card-description">
            Basic form layout
        </p>

            <!-- form start -->
            <form id="formTest" role="form" action="" class="forms-sample">
                <div class="form-group">

                    <div class="form-group">
                        <label for="title">Umur</label>
                        <select id="umur" name="umur" class="form-control form-control-lg" id="formSelectUmur">
                            <option value="1">muda</option>
                            <option value="2">dewasa</option>
                            <option value="3">tua</option>
                        </select>
                    </div>
                    
                    <div class="form-group">
                        <label for="title">Jenis Kelamin</label>
                        <select id="jk" name="jk" class="form-control form-control-lg" id="formSelectJK">
                            <option value="1">laki - laki</option>
                            <option value="2">perempuan</option>
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="title">Hasil Survei</label>
                        <select id="hsl_survei" name="hsl_survei" class="form-control form-control-lg" id="formSelectHslSurvei">
                            <option value="1">Levl A</option>
                            <option value="2">Levl B</option>
                            <option value="3">Levl C</option>
                            <option value="4">Levl D</option>
                        </select>
                    </div>
                    
                    <div class="form-group">
                        <label for="title">Jumlah Pinjaman</label>
                        <select id="jml_pinjaman" name="jml_pinjaman" class="form-control form-control-lg" id="formSelectJmlPinjaman">
                            <option value="1">A</option>
                            <option value="2">B</option>
                            <option value="3">C</option>
                            <option value="4">D</option>
                            <option value="4">E</option>
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="title">Jaminan</label>
                        <select id="jaminan" name="jaminan" class="form-control form-control-lg" id="formSelectJaminan">
                            <option value="1">jmn A</option>
                            <option value="2">jmn B</option>
                            <option value="3">jmn C</option>
                        </select>
                    </div>
                    
                    <div class="form-group">
                        <label for="title">Durasi</label>
                        <select id="durasi" name="durasi" class="form-control form-control-lg" id="formSelectDurasi">
                            <option value="1">cepat</option>
                            <option value="2">normal</option>
                            <option value="3">lama</option>
                        </select>
                    </div>
                    
                    <div class="form-group">
                        <label for="title">Modal</label>
                        <select id="modal" name="modal" class="form-control form-control-lg" id="formSelectModal">
                            <option value="1">ada</option>
                            <option value="2">tidak ada</option>
                        </select>
                    </div>
                    
                    <div class="form-group">
                        <label for="title">Ekonomi</label>
                        <select id="ekonomi" name="ekonomi" class="form-control form-control-lg" id="formSelectEkonomi">
                            <option value="1">cukup</option>
                            <option value="2">belum cukup</option>
                        </select>
                    </div>
                    
                </div>

                <button type="submit" class="btn btn-primary">Submit</button>

            </form>

            <div id="statusKlasifikasi"></div>

        </div>
    </div>

    <div id="resultKlasification"></div>

        

    <script>
        const data = {!! $datasets !!};
        // console.log(data);
    </script>

    <script src=" {{ asset('js/single.js') }} "></script>

@endsection

