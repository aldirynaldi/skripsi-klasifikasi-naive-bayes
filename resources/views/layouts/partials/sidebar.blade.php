{{-- SIDEBAR --}}
<nav class="sidebar sidebar-offcanvas" id="sidebar">
    <ul class="nav">
      <li class="nav-item">
        <a class="nav-link" href="/">
          <i class="icon-box menu-icon"></i>
          <span class="menu-title">Dashboard</span>
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="/data">
          <i class="icon-file menu-icon"></i>
          <span class="menu-title">Dataset</span>
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link" data-toggle="collapse" href="#ui-basic" aria-expanded="false" aria-controls="ui-basic">
          <i class="icon-disc menu-icon"></i>
          <span class="menu-title">Klasifikasi</span>
          <i class="menu-arrow"></i>
        </a>
        <div class="collapse" id="ui-basic">
          <ul class="nav flex-column sub-menu">
            <li class="nav-item"> <a class="nav-link" href="/kredit">Akurasi Dataset</a></li>
            <li class="nav-item"> <a class="nav-link" href="/single">kredit</a></li>
          </ul>
        </div>
      </li>
    </ul>
  </nav>
  {{-- End SIDEBAR --}}