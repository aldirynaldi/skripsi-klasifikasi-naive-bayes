//console.log(data);

import { GaussianNB } from 'ml-naivebayes';

window.document.addEventListener('DOMContentLoaded', function(){

    const arrayData = arrayObjectDBToArray(data);

    //console.log(arrayData);

    const btnSubmit = document.getElementById("formTest");

    btnSubmit.addEventListener('submit', function(event) {
        event.preventDefault();

        const inputUmur = window.document.getElementById("umur").value;
        const inputJK = window.document.getElementById("jk").value;
        const inputHslSurvei = window.document.getElementById("hsl_survei").value;
        const inputJmlPinjaman = window.document.getElementById("jml_pinjaman").value;
        const inputJaminan = window.document.getElementById("jaminan").value;
        const inputDurasi = window.document.getElementById("durasi").value;
        const inputModal = window.document.getElementById("modal").value;
        const inputEkonomi = window.document.getElementById("ekonomi").value;

        const test = [[parseInt(inputUmur), parseInt(inputJK), parseInt(inputHslSurvei), parseInt(inputJmlPinjaman), parseInt(inputJaminan), parseInt(inputDurasi), parseInt(inputModal), parseInt(inputEkonomi)]];

        console.log(test);

        const [priorYes, priorNo] = priorProbabilty(arrayData);
        console.log(priorYes);
        console.log(priorNo);
    
        const [predictions, probabiltyYes, probabilityNo] = klasifikasiNaivebayes(arrayData, test, priorYes, priorNo);

        console.log(predictions);
        console.log(probabiltyYes);
        console.log(probabilityNo);

        let status = '';
        if (predictions[0] == 1) {
            status = 'Layak';
        } else {
            status = 'Tidak Layak';
        }

        const result = document.querySelector('#resultKlasification');
        result.innerHTML = `
            <div class="card mt-4">
                <div class="card-body">
                    <div class="d-flex justify-content-between mb-3">
                        <h4 class="card-title">probabilty</h4>
                    </div>
                    <div class="row mt-2 mb-2">
                        <div class="col-6">
                        <div class=".display1"><span class="text-success">${probabiltyYes}</span> </div>
                        </div>
                        <div class="col-6">
                        <div class=".display1"><span class="text-danger">${probabilityNo}</span> </div>
                        </div>
                    </div>
                    <div id="idStatus" class="mt-5">
                        <h1>${status}</h1>
                    </div>
                </div>
            </div>
        `;
    });    

});






function arrayObjectDBToArray(arr) {

    let data = [];
    let i =0;
    arr.forEach(element => {
        data[i] = Object.values(element);
        i++;
    });

    //tahap menghilangkan id
    for (let i = 0; i < data.length; i++) {
        data[i] = data[i].slice(1, 10);
    }

    return data;
}

function pisahAtributedanLabel(arr) {
    let x = [];
    let y = [];

    for (let i = 0; i < arr.length; i++) {
        x[i] = arr[i].slice(0, 8);
        y[i] = arr[i].slice(8, 9)[0];
    }

    return [x, y];
}

// function klasifikasiNaivebayes(train, test) {

//     const [trainX, trainY] = pisahAtributedanLabel(train);

//     let model = new GaussianNB();
//     model.train(trainX, trainY);

//     let predictions = model.predict(test);

//     let probability = model.toJSON();

//     return [predictions, probability];
// }

function priorProbabilty(arr) {
    let yes = 0;
    let no = 0;
    for (let i = 0; i < arr.length; i++) {
        if (arr[i].slice(8,9) == 1) {
            yes++;
        }else if (arr[i].slice(8,9) == 0) {
            no++;
        }
    }

    yes = (yes/arr.length);
    no = (no/arr.length);

    return [yes, no];
}

function klasifikasiNaivebayes(train, test, priorA, priorB) {

    const [trainX, trainY] = pisahAtributedanLabel(train);

    let model = new GaussianNB();
    model.train(trainX, trainY);

    const probability = model.toJSON();
    const means = probability.means;
    const calculateProba = probability.calculateProbabilities;
    
    let C1_Postive = []; // 1/(sqrt(2*pi)*stdv) dan -2 * stdv * stdv  POSITIVE
    let C1_Negative = []; // 1/(sqrt(2*pi)*stdv) dan -2 * stdv * stdv  NEGATIVE
    let a = 0;
   for (let i = 1; i < calculateProba[1].length; i++) {
        C1_Postive[a] = calculateProba[1][i];
        C1_Negative[a] = calculateProba[0][i];
        a++;
   }

    let element_positive = []; // (hj - mean)^2 positif
    let element_negative = []; // (hj - mean)^2 negatif
    for (let i = 0; i < test.length; i++) {
        element_positive[i] = [];
        element_negative[i] = [];
        for (let j = 0; j < test[0].length; j++) {
            element_positive[i][j] = (test[i][j] - means[1][j]) * (test[i][j] - means[1][j]);
            element_negative[i][j] = (test[i][j] - means[0][j]) * (test[i][j] - means[0][j]);
        }
    }

   let value_positive = [];
   let value_negative = [];
   for (let i = 0; i < test.length; i++) {
       value_positive[i] = [];
       value_negative[i] = [];
       for (let j = 0; j < test[0].length; j++) {
           value_positive[i][j] = C1_Postive[j][0] * Math.exp(element_positive[i][j]/C1_Postive[j][1]);
           value_negative[i][j] = C1_Negative[j][0] * Math.exp(element_negative[i][j]/C1_Negative[j][1]);
       }
   }

   let classProbaYes = []; //(prior * likehood) positive
   let classProbaNo = []; //(prior * likehood) negative
   for (let i = 0; i < value_positive.length; i++) {
    classProbaYes[i] = priorA;
    classProbaNo[i] = priorB;
       for (let j = 0; j < value_positive[0].length; j++) {
           classProbaYes[i] *= value_positive[i][j];
           classProbaNo[i] *= value_negative[i][j];
       }
   }
    
    let evidence = [];  // evidence
    for (let i = 0; i < classProbaYes.length; i++) {
       evidence[i] = (classProbaYes[i] + classProbaNo[i]);
    }

    for (let i = 0; i < classProbaYes.length; i++) { 
        classProbaYes[i] = (classProbaYes[i]/evidence[i]); //(prior * likehood)/evidence
        classProbaNo[i] = (classProbaNo[i]/evidence[i]); //(prior * likehood)/evidence
    }

    let predictions = []; //PREDIKSINYA
    for (let i = 0; i < classProbaYes.length; i++) {
        if (classProbaYes[i] > classProbaNo[i]) {
            predictions[i] = 1;
        } else {
            predictions[i] = 0;
        }
        
    }


    return [predictions, classProbaYes, classProbaNo];
}
