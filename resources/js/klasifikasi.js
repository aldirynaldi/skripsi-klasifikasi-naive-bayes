//console.log(data);

import { GaussianNB } from 'ml-naivebayes';
const trainTestSplit = require('train-test-split');

const arrayData = arrayObjectDBToArray(data);

const X = arrayObjectToArrayX(arrayData);
const Y = arrayObjectToArrayY(arrayData);

// const normalisasi = featureScalling(X);

// const dataSiap = gabungAtributdanLabel(normalisasi, Y);

//nanti di hapus
    const dataSiap = gabungAtributdanLabel(X, Y);
//END nanti di hapus

const [train, test] = trainTestSplit(dataSiap, 0.8, 1234);
const [trainIndex, testIndex] = trainTestSplit(dataSiap, 0.8, 1234, true);

const [priorYes, priorNo] = priorProbabilty(train);

const [predictions, testY, probabilityYes, probabilityNo] = klasifikasiNaivebayes(train, test, priorYes, priorNo);

const akurasi = accuracy(predictions, testY);


makeTableTrain(train, trainIndex);
makeTableTest(test, testIndex);
makeTablePrediction(predictions);
//makeProbabiltyTable(probability);
makeakurasi(akurasi);


console.log('prediksi');
console.log(predictions);
console.log('class sebenarnya');
console.log(testY);
console.log('akurasi');
console.log(akurasi);
console.log('probabilitas');
console.log(probabilityYes);
console.log(probabilityNo);
console.log();

makeProbabiltyTable(probabilityYes, probabilityNo)

// console.log(arrayData);
// console.log(X);
// console.log(Y);
// console.log(normalisasi);
// console.log(dataSiap);
// console.log(train);
// console.log(test);

// console.log(priorYes);
// console.log(priorNo);


function arrayObjectDBToArray(arr) {

    let data = [];
    let i =0;
    arr.forEach(element => {
        data[i] = Object.values(element);
        i++;
    });

    //tahap menghilangkan id
    for (let i = 0; i < data.length; i++) {
        data[i] = data[i].slice(1, 10);
    }

    return data;
}

function arrayObjectToArrayX(arr){
    let x = [];

    for (let i = 0; i < arr.length; i++) {
        x[i] = arr[i].slice(0, 8);
    }

    return x;
}

function arrayObjectToArrayY(arr){
    let y = [];

    for (let i = 0; i < arr.length; i++) {
        y[i] = arr[i].slice(8, 9)[0];
    }

    return y;
}

function arrayObjectToArray(arr) {
    let data = [];
    let i =0;
    arr.forEach(element => {
        data[i] = Object.values(element);
        i++;
    });

    return data;
}

function priorProbabilty(arr) {
    let yes = 0;
    let no = 0;
    for (let i = 0; i < arr.length; i++) {
        if (arr[i].slice(8,9) == 1) {
            yes++;
        }else if (arr[i].slice(8,9) == 0) {
            no++;
        }
    }

    yes = (yes/arr.length);
    no = (no/arr.length);

    return [yes, no];
}

function featureScalling(arr) {

    //menjadikan array 2D ke array object
    let data = [];
    let i = 0;
    arr.forEach(element => {
        data[i] = Object.assign({}, element);
        i++;
    });

    //tahap normalisasi
    const normalize = require('feature-scaling');
    data = normalize(data);

    //mengembalikan dari array object ke array 2D
    data = arrayObjectToArray(data)

    return data;
}

function gabungAtributdanLabel(X, Y) {

    let i = 0;
    X.forEach(element => {
        element.push(Y[i]);
        i++;
    });

    return X;
    
}

function pisahAtributedanLabel(arr) {
    let x = [];
    let y = [];

    for (let i = 0; i < arr.length; i++) {
        x[i] = arr[i].slice(0, 8);
        y[i] = arr[i].slice(8, 9)[0];
    }

    return [x, y];
}

function accuracy(arr1, arr2) {
    var len = arr1.length;
    var total = 0;
    for (var i = 0; i < len; ++i) {
      if (arr1[i] === arr2[i]) {
        total++;
      }
    }
  
    return total / len;
}

function klasifikasiNaivebayes(train, test, priorA, priorB) {

    const [trainX, trainY] = pisahAtributedanLabel(train);
    const [testX, testY] = pisahAtributedanLabel(test);


    let model = new GaussianNB();
    model.train(trainX, trainY);

    // let predictions = model.predict(testX);

    const probability = model.toJSON();
    const means = probability.means;
    const calculateProba = probability.calculateProbabilities;
    
    let C1_Postive = []; // 1/(sqrt(2*pi)*stdv) dan -2 * stdv * stdv  POSITIVE
    let C1_Negative = []; // 1/(sqrt(2*pi)*stdv) dan -2 * stdv * stdv  NEGATIVE
    let a = 0;
   for (let i = 1; i < calculateProba[1].length; i++) {
        C1_Postive[a] = calculateProba[1][i];
        C1_Negative[a] = calculateProba[0][i];
        a++;
   }

    // console.log(C1_Postive);
    // console.log(C1_Negative);

    let element_positive = []; // (hj - mean)^2 positif
    let element_negative = []; // (hj - mean)^2 negatif
    for (let i = 0; i < testX.length; i++) {
        element_positive[i] = [];
        element_negative[i] = [];
        for (let j = 0; j < testX[0].length; j++) {
            element_positive[i][j] = (testX[i][j] - means[1][j]) * (testX[i][j] - means[1][j]);
            element_negative[i][j] = (testX[i][j] - means[0][j]) * (testX[i][j] - means[0][j]);
        }
    }

    // console.log(means);
    // console.log(testX);
    // console.log(element_positive);
    // console.log(element_negative);

   let value_positive = [];
   let value_negative = [];
   for (let i = 0; i < testX.length; i++) {
       value_positive[i] = [];
       value_negative[i] = [];
       for (let j = 0; j < testX[0].length; j++) {
           value_positive[i][j] = C1_Postive[j][0] * Math.exp(element_positive[i][j]/C1_Postive[j][1]);
           value_negative[i][j] = C1_Negative[j][0] * Math.exp(element_negative[i][j]/C1_Negative[j][1]);
       }
   }

//    console.log(value_positive);
//    console.log(value_negative);


   let classProbaYes = []; //(prior * likehood) positive
   let classProbaNo = []; //(prior * likehood) negative
   for (let i = 0; i < value_positive.length; i++) {
    classProbaYes[i] = priorA;
    classProbaNo[i] = priorB;
       for (let j = 0; j < value_positive[0].length; j++) {
           classProbaYes[i] *= value_positive[i][j];
           classProbaNo[i] *= value_negative[i][j];
       }
   }
    //    console.log(classProbaYes);
    //    console.log(classProbaNo);

    let evidence = [];  // evidence
    for (let i = 0; i < classProbaYes.length; i++) {
       evidence[i] = (classProbaYes[i] + classProbaNo[i]);
    }

    for (let i = 0; i < classProbaYes.length; i++) { 
        classProbaYes[i] = (classProbaYes[i]/evidence[i]); //(prior * likehood)/evidence
        classProbaNo[i] = (classProbaNo[i]/evidence[i]); //(prior * likehood)/evidence
    }

    
//    console.log(classProbaYes);
//    console.log(classProbaNo);


    let predictions = []; //PREDIKSINYA
    for (let i = 0; i < classProbaYes.length; i++) {
        if (classProbaYes[i] > classProbaNo[i]) {
            predictions[i] = 1;
        } else {
            predictions[i] = 0;
        }
        
    }

    return [predictions, testY, classProbaYes, classProbaNo];
}

function makeTableTrain(arr, idx) {
    let itemsBody = document.querySelector("#itemsTrain");
    let i = 0;
    arr.forEach( element => {
        let tr = document.createElement("tr");
    
        tr.innerHTML = `
            <td>${idx[i]+1}</td>
            <td>${element[0]}</td>
            <td>${element[1]}</td>
            <td>${element[2]}</td>
            <td>${element[3]}</td>
            <td>${element[4]}</td>
            <td>${element[5]}</td>
            <td>${element[6]}</td>
            <td>${element[7]}</td>
            <td>${element[8]}</td>
        `;
    
        itemsBody.append(tr);
    
        i++;
    });
    
}

function makeTableTest(arr, idx) {
    let itemsBody = document.querySelector("#itemsTest");
    let i = 0;
    arr.forEach( element => {
        let tr = document.createElement("tr");
    
        tr.innerHTML = `
            <td>${idx[i]+1}</td>
            <td>${element[0]}</td>
            <td>${element[1]}</td>
            <td>${element[2]}</td>
            <td>${element[3]}</td>
            <td>${element[4]}</td>
            <td>${element[5]}</td>
            <td>${element[6]}</td>
            <td>${element[7]}</td>
            <td>${element[8]}</td>
        `;
    
        itemsBody.append(tr);
    
        i++;
    });
}

function makeTablePrediction(arr) {
    let itemsHead = document.querySelector("#t_headPred");
    let tr = document.createElement("tr");

    let itemsBody = document.querySelector("#t_bodyPred");
    let tr_body = document.createElement("tr");

    let i = 0;
    arr.forEach( element => {

        let th = document.createElement('th');
        let td = document.createElement('td');
    
        th.innerText = `Testing ${i+1}`;
        tr.append(th);

        td.innerText = `${element}`;
        tr_body.append(td);

        itemsHead.append(tr);
        itemsBody.append(tr_body);
    
        i++;
    });

}

function makeProbabiltyTable(probYes, probNo) {
   
    let itemsBody = document.querySelector("#prob");

    for (let i = 0; i < probYes.length; i++) {

        let tr = document.createElement('tr');
        tr.innerHTML = `
            <td>${i+1}</td>
            <td>${probYes[i]}</td>
            <td>${probNo[i]}</td>
        `;

        itemsBody.append(tr);
    }

}

function makeakurasi(akurasi) {
    const value = document.querySelector("#akurasi");
    value.innerText = akurasi;
}

