<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::group(['middleware' => ['auth']], function () {
    //
    Route::get('/', function () {
        return view('contents.index');
    });
    
    Route::get('/data', 'DatasetController@index');
    Route::post('import', 'DatasetController@import');
    Route::post('/data/store','DatasetController@store');
    Route::get('/data/{id}/edit', 'DatasetController@edit');
    Route::put('/data/{id}', 'DatasetController@update');
    Route::delete('/data/{id}','DatasetController@destroy');
    
    Route::get('/kredit', 'DatasetController@kredit');
    Route::get('/single', 'DatasetController@single');
});

Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');
